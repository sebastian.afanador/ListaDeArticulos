var articulos = new Array();
var campos = [];
var nCampos = 0;
var n = 0;
var salida = "<div>";

function iniciar() {
    var Bienvenida = "";
    Bienvenida += "---------------------Bienvenido---------------------\n\ ";
    Bienvenida += "Asistente para creación de inventario\n";
    Bienvenida += "1.Indique cuantos campos va a introducir\n";
    Bienvenida += "2.Indique el nombre de los campos\n";
    Bienvenida += "3. Indique cuantos articulos va a introducir\n";
    Bienvenida += "4. Indique el valor de los campos para cada articulo";
    alert(Bienvenida);

    var nCampos = prompt("¿Cuantos campos introducirá?");
    for (var i = 0; i < nCampos; i++) {
        var nombre = prompt("Nombre del campo " + (i + 1));
        campos.push(nombre);
    }
}

function ingresarArticulos() {
    n = prompt("¿Cuantos articulos ingresará?");
    for (var i = 0; i < n; i++) {
        articulos.push(new Array());
        for (var j = 0; j < campos.length; j++) {
            var valor = prompt("Articulo " + (i + 1) + ":" + campos[j]);
            articulos[i].push(valor);
        }
    }
}

function construirTitulos() {
    salida += "<h1>Articulos de Inventario</h1>";
    salida += "<table border=2px>";
    salida += "<tr>";
    for (var i = 0; i < campos.length; i++) {
        salida += "<th>";
        salida += campos[i];
        salida += "</th>";
    }
    salida += "</tr>";
}

function construirTabla() {
    for (var i = 0; i < n; i++) {
        salida += "<tr>";
        for (var j = 0; j < campos.length; j++) {
            salida += "<td>";
            salida += articulos[i][j];
            salida += "</td>";
        }
        salida += "</tr>";
    }
    salida += "</table>";
    salida += "</div>";
}

iniciar();
ingresarArticulos();
construirTitulos();
construirTabla();
document.body.innerHTML = salida;
